var Game = {

	w:{ //wrappers
		chat: '#chat',
		question: '#questions',
		child: '#child',
		parent: '#parent',
		scale: '.scale',
		/*Main wrapper for scale*/
		scale_parent: "#scale",
		paint: '.logo'
	},


	step_count: 10, //Count of steps
	animate_timer: 1000, //milliseconds
	start_position: 5, //step

	answer_timer: 1500,

	/*Main init function for listeners and positions*/
	init: function(){

		/*reset positions*/
		var step_size = this.getStepSize();

		if(Start) this.start_position = 0;

		var left = step_size*this.start_position;

		if(Start)
			left = 22;
		
		$(this.w.child).animate({
			left: left,
			width: step_size
		}, 2000);

		this.paintLogo();



		/*Set listener*/
		this.setListener();

		/*Start with questions*/
		this.getQuestion();
	},

	setListener: function(){
		var self = this;

		$(this.w.question).on('click', 'a', function(e){
			if($(this).parent().is('.active')){
				self.question($(this).text(), this);
			}
			return false;
		});

		var h_a = 300; // $(".answers").height();
		var h_w = $("#main-window").height();
		$(".case-img img").height(h_w-h_a);
	},


	/*Questions is pressed*/
	question: function(text, e){

		var self = this;
		var tpl = '<div class="m-message bubble bubble-right pull-right">'+text+'</div>';
		tpl += this.clear();

		/*Append to chat*/ 
		$(this.w.chat).append(tpl);

		/*Scroll chat*/
		this.scrollChat();

		/*Disable answers*/
		$(this.w.question).removeClass('active');

		/*Get Think And Answer*/
		var data = this.getAnswer(e);
		console.log(data);
		
		/*Logic MOVE - > GAME OVER - > GLOBAL COMMENT*/
		if(!data.next && !data.answer) {
			this.comment(data.comment);
			this.gameOver();
			// return false;
		}

		/*move the Child*/
		if(!data.answer)
			self.moveChild(data.move);

		if(data.answer){
			/*Delay, and answer and think*/
			setTimeout(function(){

				/*Show Think*/
				self.think(data.answer.t);
					
					setTimeout(function(){
						
						/*clean string value of ansers false*/
						data.answer.a = data.answer.a == 'false' ? false : data.answer.a;

						/*Show answer*/
						if(data.answer.a) {
							if(data.answer.a !== true){
								self.answer(data.answer.a);
							}
						}

						/*move the Child*/
						self.moveChild(data.move);

						setTimeout(function(){
							/*Show comment*/
							if(data.comment) self.comment(data.comment);

							/*if over*/
							if(!data.next && data.answer.a !== true){
								self.gameOver();
							}

							if(data.win) 
								self.gameOver();
							else if(!data.next){
								self.gameOver();
							}

							/*Next Answers*/
							if(data.next) self.getQuestion(data.next);

						}, self.answer_timer/2);


					}, self.answer_timer);


			}, this.answer_timer/2);
		}
	},

	answer: function(text){
		var tpl = '<div class="m-message bubble bubble-left pull-left">'+text+'</div>';
		tpl += this.clear();

		/*Append to chat*/ 
		$(this.w.chat).append(tpl);

		/*Scroll chat*/
		this.scrollChat();

		/*Enable answers*/
		$(this.w.question).addClass('active');

	},

	think: function(text){
		var tpl = '<div class="think bubble">'+text+'</div>';

		/*append*/
		$(this.w.chat).append(tpl);
		this.scrollChat();
	},

	comment: function(text){
		var tpl = '<div class="comment alert alert-info">'+text+'</div>';
		$(this.w.chat).append(tpl);
		this.scrollChat();
	},

	clear: function(){
		return '<div class="clearfix"></div>';
	},

	scrollChat: function(){
		$(this.w.chat).stop().animate({scrollTop: $(this.w.chat)[0].scrollHeight}, 1000);
	},

	getStepSize: function(){
		return $(this.w.scale_parent).width() / this.step_count;
	},

	moveChild: function(step){ //-3 or +2
		/*Get cur left position*/
		var curPos = parseInt($(this.w.child).css('left')); 

		/*Get step size*/
		var step_size = this.getStepSize();

		var move = 0;

		var gameOver = false;

		var self = this;

		/*Counting*/
		if(step){
			move = curPos-(step * step_size);

			if(move<step_size) {
				// move = step_size;
				move = 22;
				// self.win();
			}
			
			if(move == $(window).width()) move = move-step_size;

			if(move > $(window).width()){
				move = $(window).width();
				gameOver = true;
			}

			// console.log('2move&step', move, step_size);
			$(this.w.child).stop().animate({left: move}, this.animate_timer, function(){
				// if(gameOver) self.gameOver();
				self.paintLogo();
				console.log('test');
			});
		}

	},

	/*Function get question*/

	getQuestion: function(i){
		i = i || 0;
		var questions = DataQuestions[i];

		/*Get questions list from DB*/
		var html = '', q, j;
		for(var i in questions){
			q = questions[i];
			
			/*If qe have some variants get Randomly*/
			if(questions[i].length){
				j = parseInt((Math.random()*(questions[i].length-1)).toFixed(0));
				q = questions[i][j];
			}
			html += "<a data-move='"+q.m+"' data-next='"+q.n+"' data-comment='"+q.c+"' data-answer='"+q.a+"' data-win='"+q.win+"'>"+q.q+"</a>\n\r";
		}

		/*Put questions on page*/
		$(this.w.question).hide().html(html);
		$(this.w.question).fadeIn();
	},


	/*Function get answer*/
	/*Get answer from clicked question*/
	getAnswer: function(e){
		var data = $(e).data();
		data.answer = DataAnswers[data.answer];

		return data;
	},

	gameOver: function(){
		var tpl = '<img src="../img/btn_again.png" class="start" onclick="location.reload();">';
		tpl += "<div style='text-align: center;' class='alert for-end'>Никогда не поздно что-то изменить, не сдавайтесь!</div>";
		// var tpl = '<div class="comment alert alert-danger">Попробуйте еще раз.</div>';
		// $("#chat").fadeOut('slow');
		$(".m-message, .think, .comment:not(:last), .for-end, .start, .end-text, .smile").remove();
		var self = this;
		
		$(this.w.chat).append(tpl);
		this.win();
		this.showEndComment();
		this.scrollChat();


	},	

	getCurStep: function(){
		var step_size = parseInt(this.getStepSize());
		var curPos = parseInt($(this.w.child).css('left'));

		var step =parseInt(curPos/step_size);
		return step;
	},

	paintLogo: function(){
		var step = this.getCurStep();
		var countOpacity = 1/step;
		
		if(countOpacity == 0.1) countOpacity = 0;

		if(!step || step==1) countOpacity = 1;

		$(this.w.paint).animate({opacity: countOpacity}, 2000);
	},

	win: function(){
		// this.gameOver();
		// return;
		// console.log('Uraa');
		// this.showEndComment();
		// this.scrollChat();
		var btn = "<a href='#'>Записаться на консультацию</a>";
			btn += "<a href='#'>Помочь получить доступную консультацию</a>";
			btn += "<a href='#'>Подписаться на обновления</a>";
		$(this.w.question).fadeOut('fast').delay(500).html(btn).fadeIn('fast');
	},

	showEndComment: function(){
		var step = this.getCurStep();
		var text = '';

		var css = "display: block; margin-left: auto; margin-right: auto;";
		if(step == 5) text = "<img src='../img/03.png' class='smile img-responsive' style='"+css+"'><br><div class='end-text' style='text-align: center;'>Ваши взаимоотношения с ребенком остались в нейтральной зоне. Нет конфликта, но нет и хорошего контакта. Сыграйте еще раз и попытайтесь найти другую линию поведения.</div><br>";
		if(step < 5 && step > 0) text = "<img src='../img/02.png' class='smile img-responsive' style='"+css+"'><br><div class='end-text' style='text-align: center;'>Предок-мечта.Вы стали ближе к своему ребенку. Спасибо, продолжайте в том же духе.</div><br>";
		if(step > 5 && step < 10) text = "<img src='../img/04.png' class='smile img-responsive' style='"+css+"'><br><div class='end-text' style='text-align: center;'>Осторожно! В результате своих действий Вы отдалили от себя ребенка. Сыграет еще раз и попробуйте что-то улучшить.</div><br>";

		if(step == 0) text = "<img src='../img/01.png' class='smile img-responsive' style='"+css+"'><br><div class='end-text' style='text-align: center;'>Поздравляем! Вы справились!</div><br>";
		if(step > 10) text = "<img src='../img/05.png' class='smile img-responsive' style='"+css+"'><br>";

		$(this.w.chat).prepend(text);
		return text;
	}
};


var DataQuestions; // = [[[{"q":"\u0414\u0430\u0432\u043d\u043e \u0441\u0438\u0434\u0438\u0448\u044c? ","a":"0","m":"0","n":"1","c":"\u041d\u0430 \u0437\u0430\u043a\u0440\u044b\u0442\u044b\u0439 \u0432\u043e\u043f\u0440\u043e\u0441 \u0431\u0443\u0434\u0435\u0442 \u043e\u0434\u043d\u043e\u0441\u043b\u043e\u0436\u043d\u044b\u0439 \u043e\u0442\u0432\u0435\u0442.","win":"false"},{"q":"\u0423\u0440\u043e\u043a\u0438 \u0441\u0434\u0435\u043b\u0430\u043d\u044b?","a":"1","m":"0","n":"1","c":"\u0417\u0430\u043a\u0440\u044b\u0442\u044b\u0439 \u0432\u043e\u043f\u0440\u043e\u0441 - \u0447\u0430\u0449\u0435 \u0432\u0441\u0435\u0433\u043e \u0432\u044b\u0437\u044b\u0432\u0430\u0435\u0442 \u043e\u0434\u043d\u043e\u0441\u043b\u043e\u0436\u043d\u044b\u0439 \u043e\u0442\u0432\u0435\u0442.","win":"false"},{"a":"9","m":"-55","n":"5","c":"Test","win":"false","q":"\u0418\u0433\u043d\u043e\u0440"}],[{"q":"\u041c\u043d\u0435 \u043d\u0443\u0436\u043d\u043e \u0441 \u0442\u043e\u0431\u043e\u0439 \u043f\u043e\u0433\u043e\u0432\u043e\u0440\u0438\u0442\u044c","a":"8","m":"1","n":"3","c":"\u0412\u044b\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0447\u0443\u0432\u0441\u0442\u0432 \u0438 \u043d\u0430\u043c\u0435\u0440\u0435\u043d\u0438\u0439 \u043e\u0442 \u043f\u0435\u0440\u0432\u043e\u0433\u043e \u043b\u0438\u0446\u0430 \u043f\u043e\u043c\u043e\u0433\u0430\u0435\u0442 \u0440\u0430\u0437\u0433\u043e\u0432\u043e\u0440\u0443","win":"false"},{"q":"\u042f \u0445\u043e\u0447\u0443 \u0442\u0435\u0431\u0435 \u0447\u0442\u043e-\u0442\u043e \u0440\u0430\u0441\u0441\u043a\u0430\u0437\u0430\u0442\u044c","a":"8","m":"1","n":"3","c":"\u0412\u044b\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0447\u0443\u0432\u0441\u0442\u0432 \u0438 \u043d\u0430\u043c\u0435\u0440\u0435\u043d\u0438\u0439 \u043e\u0442 \u043f\u0435\u0440\u0432\u043e\u0433\u043e \u043b\u0438\u0446\u0430 \u043f\u043e\u043c\u043e\u0433\u0430\u0435\u0442 \u0440\u0430\u0437\u0433\u043e\u0432\u043e\u0440\u0443","win":"false"}]],[{"q":"\u0412\u044b\u043a\u043b\u044e\u0447\u0438 \u0431\u044b\u0441\u0442\u0440\u043e!","a":"2","m":"-1","n":"2","c":"\u041f\u0440\u0438\u043a\u0430\u0437 \u0432\u044b\u0437\u044b\u0432\u0430\u0435\u0442 \u0441\u043e\u043f\u0440\u043e\u0442\u0438\u0432\u043b\u0435\u043d\u0438\u0435...","win":"false"},{"q":"\u041a\u0430\u0436\u0434\u044b\u0439 \u0434\u0435\u043d\u044c \u043e\u0434\u043d\u043e \u0438 \u0442\u043e\u0436\u0435!","a":"3","m":"-1","n":"false","c":"\u041e\u0442\u043a\u0430\u0437 \u043e\u0442 \u043e\u0442\u0432\u0435\u0442\u0441\u0442\u0432\u0435\u043d\u043d\u043e\u0441\u0442\u0438. \u0412 \u0434\u0430\u043d\u043d\u043e\u043c \u0441\u043b\u0443\u0447\u0430\u0435, \u0440\u043e\u0434\u0438\u0442\u0435\u043b\u044c \u043f\u0440\u0435\u0434\u043f\u043e\u0447\u0435\u043b \u043e\u0442\u0441\u0442\u0440\u0430\u043d\u0438\u0442\u044c\u0441\u044f \u043e\u0442 \u043f\u0440\u043e\u0431\u043b\u0435\u043c\u044b \u0438 \u043d\u0435 \u0440\u0435\u0448\u0430\u0442\u044c \u0435\u0435.","win":"false"}],[{"q":"\u0414\u0430\u0442\u044c \u043f\u043e\u0434\u0437\u0430\u0442\u044b\u043b\u044c\u043d\u0438\u043a","a":"4","m":"-4","n":"false","c":"\u0424\u0438\u0437\u0438\u0447\u0435\u0441\u043a\u043e\u0435 \u043d\u0430\u0441\u0438\u043b\u0438\u0435 \u043d\u0435 \u043f\u0440\u0438\u0435\u043c\u043b\u0435\u043c\u043e!","win":"false"},{"q":"\u0412\u044b\u0434\u0435\u0440\u043d\u0443\u0442\u044c \u043a\u043e\u043c\u043f\u044c\u044e\u0442\u0435\u0440 \u0438\u0437 \u0441\u0435\u0442\u0438","a":"5","m":"-3","n":"false","c":"\u0414\u0435\u043c\u043e\u043d\u0441\u0442\u0440\u0430\u0446\u0438\u044f \u0441\u0438\u043b\u044b \u0432\u044b\u0437\u044b\u0432\u0430\u0435\u0442 \u0430\u0433\u0440\u0435\u0441\u0441\u0438\u044e!","win":"false"},{"q":"\u041f\u043e\u0442\u043e\u043c\u0443 \u0447\u0442\u043e!","a":"6","m":"-2","n":"false","c":"\u041e\u0442\u043a\u0430\u0437 \u043e\u0442 \u043a\u043e\u043d\u0442\u0430\u043a\u0442\u0430 \u0443\u0441\u0443\u0433\u0443\u0431\u043b\u044f\u0435\u0442 \u043a\u043e\u043d\u0444\u043b\u0438\u043a\u0442!","win":"false"},{"q":"\u0421\u0438\u0434\u0435\u0442\u044c \u0434\u043e\u043b\u0433\u043e \u0432\u0440\u0435\u0434\u043d\u043e","a":"7","m":"-1","n":"false","c":"\u041c\u043e\u0440\u0430\u043b\u0438\u0437\u0430\u0442\u043e\u0440\u0441\u0442\u0432\u043e - \u043d\u0435 \u043b\u0443\u0447\u0448\u0438\u0439 \u0441\u043f\u043e\u0441\u043e\u0431 \u0432\u0435\u0434\u0435\u043d\u0438\u044f \u0431\u0435\u0441\u0435\u0434\u044b","win":"false"}],[{"q":"\u042f \u043d\u0435 \u043f\u043e\u043d\u0438\u043c\u0430\u044e, \u043f\u043e\u0447\u0435\u043c\u0443 \u0442\u044b \u0441\u0442\u043e\u043b\u044c\u043a\u043e \u0432\u0440\u0435\u043c\u0435\u043d\u0438 \u043f\u0440\u043e\u0432\u043e\u0434\u0438\u0448\u044c \u0437\u0430 \u043a\u043e\u043c\u043f\u044c\u044e\u0442\u0435\u0440\u043e\u043c?","a":"10","m":"-1","n":"2","c":"\u0412\u043e\u043f\u0440\u043e\u0441 \u0432 \u0432\u0438\u0434\u0435 \u043e\u0431\u0432\u0438\u043d\u0435\u043d\u0438\u044f \u0432\u0435\u0434\u0451\u0442 \u043a \u043a\u043e\u043d\u0444\u043b\u0438\u043a\u0442\u0443.","win":"false"},{"q":"\u041c\u044b \u0441 \u0442\u043e\u0431\u043e\u0439 \u0434\u0430\u0432\u043d\u043e \u043d\u0435 \u0440\u0430\u0437\u0433\u043e\u0432\u0430\u0440\u0438\u0432\u0430\u043b\u0438","a":"11","m":"1","n":"4","c":"\u0420\u0430\u0432\u0435\u043d\u0441\u0442\u0432\u043e \u043f\u043e\u0437\u0438\u0446\u0438\u0439 \u0438 \u043e\u0442\u043a\u0440\u044b\u0442\u043e\u0441\u0442\u044c \u0432 \u0440\u0430\u0437\u0433\u043e\u0432\u043e\u0440\u0435 \u043f\u043e\u043c\u043e\u0433\u0430\u044e\u0442 \u043f\u0440\u0438\u0432\u043b\u0435\u0447\u044c \u0432\u043d\u0438\u043c\u0430\u043d\u0438\u0435 \u0440\u0435\u0431\u0451\u043d\u043a\u0430.","win":"false"},[{"q":"\u0420\u0430\u0441\u0441\u043a\u0430\u0437\u0430\u0442\u044c \u043e \u0440\u0430\u0431\u043e\u0442\u0435","a":"9","m":"-1","n":"false","c":"\u041f\u043e\u0434\u0440\u043e\u0441\u0442\u043a\u0430 \u043c\u0430\u043b\u043e \u0438\u043d\u0442\u0435\u0440\u0435\u0441\u0443\u044e\u0442 \u0442\u0435\u043c\u044b, \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u0435\u0433\u043e \u043d\u0435 \u043a\u0430\u0441\u0430\u044e\u0442\u0441\u044f, \u043e\u0441\u043e\u0431\u0435\u043d\u043d\u043e \u0435\u0441\u043b\u0438 \u0432\u0430\u0436\u043d\u044b\u0435 \u0435\u043c\u0443 \u0442\u0435\u043c\u044b \u0438\u0433\u043d\u043e\u0440\u0438\u0440\u0443\u0440\u044e\u0442\u0441\u044f","win":"false"},{"q":"\u0420\u0430\u0441\u0441\u043a\u0430\u0437\u0430\u0442\u044c \u043e \u0441\u0435\u043c\u044c\u0435","a":"9","m":"-1","n":"false","c":"\u041f\u043e\u0434\u0440\u043e\u0441\u0442\u043a\u0430 \u043c\u0430\u043b\u043e \u0438\u043d\u0442\u0435\u0440\u0435\u0441\u0443\u044e\u0442 \u0442\u0435\u043c\u044b, \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u0435\u0433\u043e \u043d\u0435 \u043a\u0430\u0441\u0430\u044e\u0442\u0441\u044f, \u043e\u0441\u043e\u0431\u0435\u043d\u043d\u043e \u0435\u0441\u043b\u0438 \u0432\u0430\u0436\u043d\u044b\u0435 \u0435\u043c\u0443 \u0442\u0435\u043c\u044b \u0438\u0433\u043d\u043e\u0440\u0438\u0440\u0443\u0440\u044e\u0442\u0441\u044f","win":"false"}]],[{"q":"\u042f \u0445\u043e\u0447\u0443 \u0441\u043a\u0430\u0437\u0430\u0442\u044c \u0442\u0435\u0431\u0435, \u0447\u0442\u043e \u0441\u0438\u043b\u044c\u043d\u043e \u0432\u043e\u043b\u043d\u0443\u044e\u0441\u044c, \u0447\u0442\u043e \u0442\u044b \u0441\u0442\u043e\u043b\u044c\u043a\u043e \u0432\u0440\u0435\u043c\u0435\u043d\u0438 \u043f\u0440\u043e\u0432\u043e\u0434\u0438\u0448\u044c \u0437\u0430 \u043a\u043e\u043c\u043f\u044c\u044e\u0442\u0435\u0440\u043e\u043c","a":"12","m":"1","n":"false","c":"\u0420\u0435\u0431\u0451\u043d\u043a\u0443 \u0432\u0430\u0436\u043d\u043e \u0447\u0443\u0432\u0441\u0442\u0432\u043e\u0432\u0430\u0442\u044c \u0441\u0432\u043e\u044e \u0437\u043d\u0430\u0447\u0438\u043c\u043e\u0441\u0442\u044c \u0434\u043b\u044f \u0440\u043e\u0434\u0438\u0442\u0435\u043b\u044f.","win":"true"},{"q":"\u0414\u0430\u0432\u0430\u0439 \u0434\u043e\u0433\u043e\u0432\u043e\u0440\u0438\u043c\u0441\u044f, \u043a\u0430\u043a \u0442\u044b \u0431\u0443\u0434\u0435\u0448\u044c \u0441\u0438\u0434\u0435\u0442\u044c \u0437\u0430 \u043a\u043e\u043c\u043f\u044c\u044e\u0442\u0435\u0440\u043e\u043c. \u0427\u0442\u043e \u0434\u0443\u043c\u0430\u0435\u0448\u044c?","a":"14","m":"2","n":"false","c":"\u041f\u0430\u0440\u0442\u043d\u0451\u0440\u0441\u043a\u0430\u044f \u043f\u043e\u0437\u0438\u0446\u0438\u044f \u043f\u043e\u043c\u043e\u0433\u0430\u0435\u0442 \u0443\u0441\u0442\u0430\u043d\u043e\u0432\u0438\u0442\u044c \u0438 \u0437\u0430\u043a\u0440\u0435\u043f\u0438\u0442\u044c \u043a\u043e\u043d\u0442\u0430\u043a\u0442 \u0441 \u0440\u0435\u0431\u0451\u043d\u043a\u043e\u043c.","win":"true"}],[{"q":"\u042d\u0442\u043e \u043d\u043e\u0432\u044b\u0439 \u0431\u043b\u043e\u043a","win":"false","c":"\u0435\u0443\u044b\u0435","n":"0","m":"+5","a":"0"}],[{"q":"\u0430\u0440\u043f\u0432\u0430\u043f\u0432\u0430\u043f\u0432\u0430\u043f","win":"false","c":"\u0432\u0430\u043f\u0432\u0430\u043f","n":"2","m":"4","a":"2"}]];

var DataAnswers; /*[
	{a: "Два часа", t: "Опять хочет следить за каждым шагом"},
	{a: "Не все", t: "Никакой свободы"},
	{a: "Почему?", t: "Опять командует. Надоело!"},
	{a: false, t: "Начинается!"},
	{a: false, t: "Мне страшно..."},//4
	{a: false, t: "Скорее бы вырасти и отомстить!"},//5
	{a: false, t: "Всем плевать на моё мнение"},//6
	{a: false, t: "А жить вообще вредно!"},//7
	{a: "Ммм", t: "Неужели?"},//8
	{a: "Ты всё?", t: "Ну круто, а мне что?"},//9
	{a: "Все так делают! Все мои друзья так делают! Почему мне нельзя?", t: "Опять, ну сколько можно!"},//10
	{a: "Ну, да", t: "Ок, давай!"},//11
	{a: true, t: "Я важен, за меня волнуются"},//12
	{a: true, t: "Всегда есть что-то важнее меня!"},//13
	{a: true, t: "Моё мнение учитывается!"},//14
];*/


$(document).ready(function(){

	var c = "case_1";

	if(!Start){
		$.getJSON('/ajaxAdmin/db/'+c+'.questions.json', function(q){
			DataQuestions = q;
			$.getJSON("/ajaxAdmin/db/" + c + ".answers.json", function(a){
				DataAnswers = a;
				$(".menu-a").fadeIn('slow', function(){
					$("#p2").width($(".header").width()-$("#p1").width());		
					Game.init();
				})
			});
		});
	}else{
		$("#p2").width($(".header").width()-$("#p1").width());		
			Game.init();
	}

	// $("#p2").width($(".header").width()-$("#p1").width());		
	// Game.init();

	$("#main-window, #chat").height($(window).height()-135);
	
})