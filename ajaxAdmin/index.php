<?php
	$authorized = 0;

	session_start();

	if(isset($_SESSION['auth']))
		$authorized = true;
	else{
		if(isset($_GET['p'])){
			if($_GET['p'] == 'blizkie2019'){
				$_SESSION['auth'] = true;
			}
			header('location: /ajaxAdmin');
		}else{
			echo "<script>var p = prompt(\"Password\");
			if(p){
				location.search='p='+p;
			}</script>";
		}
	}

	/*Auth*/
	if(!$authorized) die('No access');

	/*Include logic file*/
	require_once("inc/handler.php");

?>

<?php include("tpl/header.php");?>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="form-group">
				<label>Выбор кейса</label>
				<select class="form-control" name='case'>
					<option value="">--Выбрать кейс--</option>
					<option value="case_1">1. Подросток много времени проводит с гаджетами (мобильным телефоном, планшетом, компьютером)</option>
					<option value="case_2">2. У подростка появились странные посты в социальных сетях (суицидальные, агрессивные и т.п.)</option>
					<option value="case_3">3. Подросток потерял интерес к еде</option>
					<option value="case_4">4. Подросток не доволен своей внешностью</option>
					<option value="case_5">5. У подростка ухудшились оценки</option>
					<option value="case_6">6. Подросток на все реагирует излишне эмоционально, часто плачет</option>
					<option value='case_7'>7. Подросток грубит, хлопает дверью</option>
					<option value='case_8'>8. Подросток стал замкнутым, подавленным</option>
					<option value='case_9'>9. Подросток говорит, что он никому не нужен или что он хочет исчезнуть</option>
					<!-- new 2021 -->
					<option value="case_10">10. Подросток познакомился с новой компаний, где все ездят на велосипедах/самокатах. Просит купить ему такой же</option>
					<option value="case_11">11. Подросток отпрашивается съездить с друзъями на природу с ночевкой </option>
					<option value="case_12">12. Подросток пришел грязный и с порванными вещами. Опять. Похоже, в школе буллинг</option>
					<option value="case_13">13. Подросток хочет сделать тату и покрасить волосы в изумрудный цвет</option>
					<option value="case_14">14. Подросток приходит с прогулки все позже, не сообщает куда идет </option>
					<option value="case_15">15. Подросток подолгу переписывается с кем-то в соц сетях, телефон запаролен</option>
					<option value="case_16">16. Подросток впервые влюбился </option>
					<option value="case_17">17. Подросток пришел домой с признаками алкогольного опьянения и запахом сигарет</option>
					<option value="case_18">18. Подросток говорит, что у них отменили занятия, но вы знаете, что это не правда</option>

				</select>

				<div id="playground"></div>
				<br>
				<button class='btn btn-info event_add' data-fn='addNewSticker'><span class='glyphicon glyphicon-plus'></span> Добавить блок</button>
				<button class='btn btn-warning' id='open_answers'><span class='glyphicon glyphicon-edit'></span> Ответы</button>
				<button class='btn btn-success disabled event_add' disabled="disabled" data-fn='save' id='save'><span class='glyphicon glyphicon-floppy-disk'></span> Сохранить</button>

			</div>
		</div>

		<div class="col-sm-12">
			<form class='answer-form'>
				<div class='answers'>
					<div class="content"></div>
					<div class="btn btn-success event_add" data-fn='addAnswer'><span class='glyphicon glyphicon-plus'></span> Добавить ответ</div>
				</div>
			</form>
		</div>

		<div class="col-sm-12">
			<div id='sticker_wrapper'>
				загрузка...
				
			</div>
		</div>

	</div>	
</div>

<div class="modal fade" tabindex="-1" role="dialog" id='modal-q'>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Добавить вопрос</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
        	<textarea class="form-control" placeholder="Текст вопроса"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" id=>Добавить вопрос</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>


var  Sticker = {
	e: {
		sticker : "<div class='col-md-4'><div class='sticker'><form class='sticker_form'><input type='hidden' name='sticker-id' value='1'></form><div class='btns'><div class='btn btn-sm btn-danger event_add'><span class='glyphicon glyphicon-minus'></span></div><div class='btn btn-sm btn-warning event_add' data-fn='addNewQuestion'><span class='glyphicon glyphicon-plus'></span></div></div><div class='groups'><div class='close'><span aria-hidden='true'>×</span></div><div class='content'></div><button class='btn btn-success event_add' data-fn='groupAdd'><span class='glyphicon glyphicon-plus'></span></button></div></div></div>",
		questionField : "<div class='question'><div class='input-group'><span class='input-group-addon'><span class='glyphicon glyphicon-th-large event_add' data-fn='addToGroup'></span></span><input type='text' placeholder='Текст вопроса' class='form-control' name='q[]'><span class='input-group-btn'><button class='btn btn-default settings event_add' data-fn='showProperties' type='button'><span class='glyphicon glyphicon-cog'></span>&nbsp;</button></div></span></div>",
		wrapperProp : "<div class='properties' style='display:none'><div class='close'><span aria-hidden='true'>×</span></div><form class='prop_form'><input type='hidden' name='field'><div class='form-group'><label>Ответ:</label><select class='form-control' name='a'><option disabled='disabled' selected='selected'>-Ответ-</option></select></div><div class='form-group'><label>Балл:</label><input placeholder='Значение балла (+1 или -2..)' class='form-control' type='text' name='m'></div><div class='form-group'><label>Следующий вопрос:</label><select class='form-control' name='n'><option disabled='disabled' selected='selected'>-Следующий вопрос-</option></select></div><div class='form-group'><label>Комментарий:</label><input placeholder='Комментарий' class='form-control' type='text' name='c'></div><div class='form-group'><label><input type='checkbox' name='win'>Победа</label></div></form></div>",
		answerField : "<div class='answer-wrapper row'><div class='col-sm-6 answer'><div class='form-group'><label>Ответ:</label><input type='text' class='form-control' name='a'></div></div><div class='col-sm-6 think'><div class='form-group'><label>Мысль:</label><input type='text' class='form-control' name='t'></div></div></div>",
		wrapperAnswer: '.answers .content',
		wrapper : "#sticker_wrapper",
		wrapperAdditionalQ: ".additional_questions",
		btn_add : ".event_add",
	},

	data: false,

	init : function(){
		var self = this;

		/*Clear the workspace*/
		$(this.e.wrapper).empty();

		/*Load data and parse*/
		this.loadDB();

		/*Load answers*/
		this.loadAnswers();

		/*event click add btn*/
		$("body").unbind('click');
		$("body").on('click', this.e.btn_add, function(e){
			var fn = $(this).data('fn');

			if(!fn) return alert('Please add "data-fn"');
			if(!self[fn]) return alert('No detected function '+fn);
			
			self[fn](this);
			
		});


		/*Save btn*/
		$("#save").attr('disabled', 'disabled').addClass('disabled');
		$("body").on('change', ".sticker select, .sticker :text, .sticker :checkbox, .answer :text", function(){
			$("#save").removeAttr('disabled').removeClass('disabled');
		});

		/*Event change select case*/
		$('select[name=case]').unbind('change');
		$('select[name=case]').on('change', function(){
			self.init();
		});

		/*close*/
		$("body").on('click', ".close", function(){
			$(this).parent().animate({
				// right: -300,
				width: 0
			}, 600, function(){
				$(this).hide();
			})
		});
	},

	addAnswer: function(e){
		$(this.e.wrapperAnswer).append(this.e.answerField);
	},

	addNewSticker : function(){
		/*Get ID*/
		var id = $(this.e.wrapper).find('.sticker').length;

		/*Add id and prepare sticker*/
		var sticker = $(this.e.sticker);

		sticker.find('[name=sticker-id]').val(id);
		$(this.e.wrapper).append(sticker);

		/*LoadProperties*/
		this.LoadProperties(id);
	},

	LoadProperties : function(id){
		var sticker = $(this.e.wrapper).find(':hidden[name=sticker-id][value='+id+']').parents(".sticker");

		sticker.append(this.e.wrapperProp);
	},

	getProperties : function(target, prop_window){
		var q = $(target).parents('.question');
		var data = q.data();

		//console.log('get question data', data);

		/*clean previews data*/
		prop_window.find('form.prop_form')[0].reset();

		/*get props*/
		var e;
		for(var i in data){
			e = prop_window.find("[name="+i+"]");
			
			/*checkbox*/
			if(e.is(':checkbox')){
				if(data[i] == true){
				 	e.prop('checked', 'checked');
				 	console.log(data[i], "true");
				}
			}

			/*text*/
			if(e.is(':text')){
				e.val(data[i]);
			}

			/*select*/
			if(e.is('select')){
				if(data[i] || data[i] == '0'){
					e.find('option[value='+data[i]+']').prop('selected', true);
				}
			}
		}
	},

	getPropSelect: function(){
		//looking for answers
		$(".properties select[name=a]").find('option:gt(0)').remove();
		var opt;
		for(var i in this.answers){
			opt = "<option value='"+i+"'>" + this.answers[i].a + "/" +this.answers[i].t+ "</option>";
			$('.properties').find('select[name=a]').append(opt);
		}

		//looking for questions
		$(".properties select[name=n]").find('option:gt(0)').remove();

		var count = 0, first_text;
		$(".sticker").each(function(i){
			count++;
			first_text = $(this).find(":text:first").val();

			$(".properties").find('select[name=n]').append("<option value='"+i+"'>Блок "+count+" ("+first_text+")</option>");

		});
	},

	/*Show properties window*/
	showProperties : function(e){
		var sticker = $(e).parents('.sticker');
		var prop_window = sticker.find('.properties');

		/*set field*/
		this.selectedField = $(e).parents(".question");

		var close = sticker.find('.properties').is(':hidden');

		var t = this;

		/*Close other*/
		$('.properties').css({width:0, right:0}).hide();

		if(!close){
			$(e).parents('.sticker').find(".properties").attr('data-flag', 'close');
		}else{
			prop_window.attr('data-flag', 'open').show().animate({
				width: 300,
				// right: -300
			}, 600);

			/*load params*/
			this.getProperties(e, prop_window);

			/*Properties event*/
			$(".properties").find(':text, select, :checkbox').unbind('change');
			$(".properties").find(':text, select, :checkbox').on('change', function(e){
				t.saveProperties(this);
			});
		}

	},

	saveProperties : function(e){
		var prop_window = $(e).parents('.properties');

		/*Get current field*/
		var field = this.selectedField;

		if(field.length){
			var v, name, checked;
			prop_window.find(':text, :checkbox, select').each(function(f){
				v = $(this).val();
				name = $(this).attr('name');

				//additional check for checkbox
				if(name == 'win'){
					v = prop_window.find(':checkbox').is(':checked');
				}

				field.data(name, v);

			});
		}
	},

	addNewQuestion: function(e){
		var sticker = $(e).parents('.sticker');
		
		$(sticker).find('form.sticker_form').append(this.e.questionField);
		this.getPropSelect();
	},

	addQuestionVariant : function(e){
		var properties = $(e).parents('.properties');
		properties.find(this.e.wrapperAdditionalQ).find(".event_add").before("<input placeholder='Вариант вопроса' class='form-control' type='text' name='_q[]'>");
	},

	/*loading answers*/
	loadAnswers : function(){
		this.loadDB('answers');
	},

	loadDB :  function(type){

		/*Get type*/
		type = type || 'questions';

		var case_name = $("[name=case]").val();
		var t = this;

		if(case_name){
			var fileName = "./db/"+case_name+"."+type+".json";
			t.fileName = fileName;

			/*Check file is created*/
			$.getJSON(fileName, function(e){

				/*loaded*/
				t.readData(fileName, function(data){
					if(type == 'questions'){
						t.questions = data;
						t.parseData(data);
					}else{
						t.answers = data;
						t.parseData(data, true);
					}
				});

			}).fail(function(e){
				if(e.status == 404){ //Not Found
					t.writeData(fileName, "{}", function(r){
						t.loadDB();
					});
				}else{// Not in JSON format
					alert('Проблема загрузки базы, возможно она повреждена');
				}
			});
		}

		return true;

	},

	parseData : function(data, custom){
		var is_answer = custom || false;
		var t = this;

		console.log('this is data came for parse', data);
		
		if(is_answer){
			$(t.e.wrapperAnswer).empty();

			if(data){
				/*cycle all answers and add HTML to wrapper*/
				for(var i in data){
					$(t.e.wrapperAnswer).append(t.e.answerField);
					$(t.e.wrapperAnswer).find('.answer:last').parent().attr('data-id', i);
					$(t.e.wrapperAnswer).find('.answer:last [name=a]').val(data[i].a);
					$(t.e.wrapperAnswer).find('.think:last [name=t]').val(data[i].t);
				}
			}

		}else{	
			var sticker, group, group_class, group_title;
			if(data){
				for(var i in data){ // i this is sticker id
					
					/*Create sticker*/
					t.addNewSticker();

					/*Get last sticker*/
					sticker = $('.sticker:last');

					/*Add data to sticker*/
					for(var j in data[i]){ // j is question id
						
						/*Sticker add input*/
		
						if(data[i][j].length){ //This is for groups

							/*Create group wrapper*/
							group_class = 'group-'+j;
							group_title = "Стикер " + i + ", группа "+j
							sticker.find("form.sticker_form").append("<div class='group "+group_class+"'><span class='group-title'>" + group_title + "</span></div>");
							group = sticker.find("."+group_class);


							for(var k in data[i][j]){
								
								/*Add input to group*/
								group.append(t.e.questionField);
								
								/*Add text*/
								group.find('.question :text:last').val(data[i][j][k].q);

								/*Add params*/
								t.addQuestionParams(group.find('.question:last'), data[i][j][k]);
							}
						}else{//For single question
							t.addNewQuestion(sticker.find('form'));
							sticker.find('.question :text:last').val(data[i][j].q);

							/*Add params*/
							t.addQuestionParams(sticker.find(".question:last"), data[i][j]);
						}

					}

				}
			}
		}

		/*Load properties select*/
		t.getPropSelect();
	},

	addQuestionParams: function(target, data){
		
		for(var i in data){
			$(target).attr("data-"+i, data[i]);
		}
	},

	writeData : function(file, data, callback){
		
		$.post('./write.php', {filename: file, data: data}, function(e){
			callback(e);
		});

		return;
	},

	readData : function(file, callback){
		$.get('./read.php', {filename: file}, function(e){
			if(e){
				e = JSON.parse(e);
			}
			callback(e);
		});

		return;
	},

	/*AddToGroup function*/
	addToGroup : function(e){
		var sticker = $(e).parents('.sticker');
		var wrapper = sticker.find('.groups');
		var sticker_id = sticker.find('[name=sticker-id]').val();

		/*Load groups from sticker*/
		var groups = sticker.find('.group');

		var chbx;
		wrapper.find('.content').empty();
		groups.each(function(){
			chbx = "<div><label><input type='radio' value='"+$(this).attr('class').replace("group ", "")+"' name='group'> "+$(this).find('.group-title').text() + "</label></div>";
			wrapper.find('.content').append(chbx);
		});

		wrapper.find('.content').append("<div><label><input type='radio' value='uncheck' name='group'> Убрать из групп</label></div>")

		/*select radio*/
		var field = $(e).parents('.group');
		/*If in a group*/
		if(field.length){
			var group_id = field.attr('class').replace("group ", "");
			wrapper.find(':radio[value='+group_id+']').prop('checked', true);
		}

		var cur_el = $(e).parents('.question');

		var group_selected;
		wrapper.find(':radio').unbind('change');
		wrapper.find(':radio').on('change', function(){
			group_selected = $(this).val();

			if(group_selected != 'uncheck')
				sticker.find(".group."+group_selected).append(cur_el);
			else{
				sticker.find('.sticker_form').prepend(cur_el);
			}

			wrapper.hide();
		});

		wrapper.css("width", "90%").fadeIn();
	},

	/*AddGroup*/
	groupAdd: function(e){
		var sticker = $(e).parents('.sticker');
		var s_c = sticker.find('[name=sticker-id]').val();
		var g_c = sticker.find('.group').length+1;
		var className = "group group-"+g_c;
		var html = "<div class='"+className+"'><div class='group-title'>Стикер "+s_c+", группа "+g_c+"</div></div>";

		sticker.find('.sticker_form').append(html);

		this.addToGroup(e);
	},

	/*Save function*/
	save: function(){

		/*Get data*/
		var questions = $('.sticker');
		var answers = $('.answer-wrapper');

		/*Questions block (sticker)*/
		var QuestionData = [];
		var q, group, s, q_data; //q - question, group - [], s - sticker id
		questions.each(function(i){
			
			s = $(this).find("[name=sticker-id]").val();

			/*Get single questions*/
			q = $(this).find('.sticker_form>.question');

			/*Check data*/
			q_data = [];
			var _q;
			q.each(function(){
				//push data
				if($(this).find(":text").val().length){
					_q = $(this).data();
					_q.q = $(this).find(":text").val();
					q_data.push(_q);
				}
			});


			/*Get Groups questions*/
			var _g;
			$(this).find('.group').each(function(){
				var group = [];
				$(this).find('.question').each(function(){
					if($(this).find(":text").val().length){
						_g = $(this).data();
						_g.q = $(this).find(":text").val();
						group.push(_g);
					}
				});
				q_data.push(group);
			});

			QuestionData.push(q_data);
		});

		console.log("QuestionData", QuestionData);

		var case_name = $("[name=case]").val();

		/*Get Answers*/
		var AnswerData = [], a, t;
		$(".answer-wrapper").each(function(){
			a = $(this).find('.answer :text').val() || false;
			t = $(this).find('.think :text').val();

			// console.log(t);

			if(t){
				AnswerData.push({a: a, t: t});
			}

		});

		var t = this;
		/*Save data*/
		this.writeData("./db/" + case_name + ".questions.json", QuestionData, function(){
			
			/*save answers*/
			t.writeData('./db/' + case_name + '.answers.json', AnswerData, function(){
				t.init();
			});

		});


	}
}

Sticker.init();
	
	$("#open_answers").on('click', function(){
		
		$(".answer-form").slideToggle();
	});

	
	function readData(file){

	}

	function writeData(file){

	}

</script>

<?php include("tpl/footer.php");?>