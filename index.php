<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Близкие - тренажер для родителей подростков</title>

    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">


    <style>
        #author {
            background: #fff;
            margin-left: -12px;
            padding: 15px;
            font-size: 0.8em;
        }

        .cardin .text2 {
            position: absolute;
            bottom: 90px;
            margin: 0 auto;
            left: 0;
            right: 0;
            font-size: 1.2em;
            font-family: "DIN Pro Bold";
            color: #fff;
            text-shadow: 2px 2px 3px black;
            background: #dddddda6;
            margin: 160px;
        }
    </style>


    <script src='/js/jquery.js'></script>

    <script type="text/javascript">
        var Start = true;
        var c = false;

        function set_gender(v) {

            var a = "#instruction, #select-gender";
            var b = "#select-item, #change-gender";

            if (v == 'show') { //show btn change gender

                $(b).fadeOut("fast");
                $(a).fadeIn("fast");
                return;
            }

            $("#change-gender img").attr('src', "/png/round-" + v + ".png");

            $(a).slideUp("fast");
            $(b).slideDown("fast", function() { //achieve



                $(".cards").each(function() {
                    var c = $(this).data('case');
                    if (c) {
                        if (localStorage[c] == '0') {
                            $("." + c + " .achive .n").fadeOut(1000);
                            $("." + c + " .achive .y").fadeIn(1500);
                        }

                        if (localStorage[c + "_visited"]) {
                            $("." + c).find('img').removeClass('grayscale');
                        }
                    }
                });

            });

            localStorage.setItem('gender', v);
            return true;
        }

        $(document).ready(function() {
            /*check selected gender*/
            if (localStorage.gender) {
                set_gender(localStorage.gender);
            }

            $(".marged").css("margin-top", $(".header").height() + 10);

            $(".cards").on('click', function() {
                setVisited(this);
            })

        });

        function setVisited(e) {
            var _case = $(e).data('case') + "_visited";
            localStorage.setItem(_case, 'visited');
        }
    </script>

    <script src='js/bootstrap.min.js'></script>
    <script src='js/jquery.easing.1.3.js'></script>
    <script src='js/game.js'></script>

</head>

<body>

    <div class='header p-1 scale-wrapper'>
        <div class='row'>
            <div class='col-md-8'>
                <table>
                    <tr>
                        <td id='p1'>
                            <div class='left-header'>
                                <img src='img/logo.png' class='logo'>
                            </div>
                        </td>
                        <td id='p2'>
                            <div id='scale'>

                                <div id='pointer'>
                                    <div id='parent'></div>
                                    <div id='child'></div>
                                </div>

                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <div class='col-md-4' style="z-index: 9999; padding: 18px; text-align: center;">
                <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                <script src="//yastatic.net/share2/share.js"></script>
                <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,whatsapp,skype,telegram"></div>
            </div>

        </div>
    </div>



    <div class="container marged">
        <section class="row" id="change-gender" style="display: none">
            <div class='col-md-12'>
                <a href="javascript:void(0);" onclick="set_gender('show');" class='btn btn-sm btn-default'>
                    <img src="/png/round-m.png" style='width: 35px;  filter: grayscale(100%);'> Играть за другого родителя
                </a>
            </div>
        </section>
        <section id="instruction">
            <div class="row">
                <div class="col-md-12">
                    <div class="instruction">
                        <p class='intence'>В этой игре вы сможете прокачать свои родительские навыки. Посмотрим, удастся ли вам стать супер-предком?</p>
                        <p>Мы собрали самые типичные ситуации в отношениях с подростками. От жизни это отличается тем, что помимо ответов подростка вы будете видеть его мысли и комментарии психолога. А ещё вы сможете увидеть, как каждая ваша реплика или
                            действие делают вас ближе или отделяют подростка от вас. И при необходимости - отыграть всю ситуацию сначала.</p>
                        <p>Помните: конкретные слова в вашем реальном общении с подростком могут отличаться от тех, что вы встретите в игре. Но понимание принципов общения поможет вам получить лучшие результаты и в игре, и в жизни. Ну что, начнем?</p>
                    </div>
                </div>
            </div>
        </section>

        <section id='select-gender'>
            <h2 style='text-align: center; font-weight: normal; color: #fff;'>Кто вы?</h2>
            <div class="row">
                <div class="col-md-6 cards">
                    <a href='javascript:void(0);' onclick="set_gender('w'); return false;" class="cardin" style="background: transparent; height: auto;">
                        <!-- <img src="./png_new/mother2.png" class='img-responsive' style="height: 100%"> -->
                        <img src="./png_new_wb/Mother_2.png" class='img-responsive' style="height: 100%">
                        <p class='text2'>Я мама</p>
                    </a>
                </div>

                <div class="col-md-6 cards">
                    <a href='javascript:void(0);' onclick="set_gender('m'); return false;" class="cardin" style="background: transparent; height: auto;">
                        <!-- <img src="./png_new/father2.png" class='img-responsive' style="height: 100%"> -->
                        <img src="./png_new_wb/Father_2.png" class='img-responsive' style="height: 100%">
                        <p class='text2'>Я папа</p>
                    </a>
                </div>

            </div>
        </section>

        <section id='select-item' style="display: none;">
            <h2 style='text-align: center; font-weight: normal; color: #fff;'>Выберите ситуацию.</h2>
            <div class="row">
                <div class="col-md-4 cards case_1" data-case='case_1'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/1-view.html' class="cardin">
                        <img src="./png_new/case1.png" class='img-responsive grayscale'>
                        <p class='text'>Подросток много времени проводит с гаджетами (мобильным телефоном, планшетом, компьютером)</p>
                    </a>
                </div>
                <div class="col-md-4 cards case_2" data-case='case_2'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/2-view.html' class="cardin">
                        <img src="./png_new/case2.png" class='img-responsive grayscale'>
                        <div class='text'>У подростка появились странные посты в социальных сетях (суицидальные, агрессивные и т.п.)</div>
                    </a>
                </div>
                <div class="col-md-4 cards case_3" data-case='case_3'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/3-view.html' class="cardin">
                        <img src="./png_new/case3.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток потерял интерес к еде</div>
                    </a>
                </div>
                <div class="col-md-4 cards case_4" data-case='case_4'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/4-view.html' class="cardin">
                        <img src="./png_new/case4.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток не доволен своей внешностью</div>
                    </a>
                </div>
                <div class="col-md-4 cards case_5" data-case='case_5'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/5-view.html' class="cardin">
                        <img src="./png_new/case5.png" class='img-responsive grayscale'>
                        <div class='text'>У подростка ухудшились оценки</div>
                    </a>
                </div>
                <div class="col-md-4 cards case_6" data-case='case_6'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/6-view.html' class="cardin">
                        <img src="./png_new/case6.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток на все реагирует излишне эмоционально, часто плачет</div>
                    </a>
                </div>
                <div class="col-md-4 cards case_7" data-case='case_7'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/7-view.html' class="cardin">
                        <img src="./png_new/case7.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток грубит, хлопает дверью</div>
                    </a>
                </div>
                <div class="col-md-4 cards case_8" data-case='case_8'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/8-view.html' class="cardin">
                        <img src="./png_new/case8.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток стал замкнутым, подавленным</div>
                    </a>
                </div>
                <div class="col-md-4 cards case_9" data-case='case_9'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/9-view.html' class="cardin">
                        <img src="./png_new/case9.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток говорит, что он никому не нужен или что он хочет исчезнуть</div>
                    </a>
                </div>

                <!-- 2021 -->
                <div class="col-md-4 cards case_10" data-case='case_10'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/10-view.html' class="cardin">
                        <img src="./png_new/case10.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток познакомился с новой компаний, где все ездят на велосипедах/самокатах. Просит купить ему такой же</div>
                    </a>
                </div>

                <div class="col-md-4 cards case_11" data-case='case_11'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/11-view.html' class="cardin">
                        <img src="./png_new/case11.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток отпрашивается съездить с друзъями на природу с ночевкой</div>
                    </a>
                </div>

                <div class="col-md-4 cards case_12" data-case='case_12'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/12-view.html' class="cardin">
                        <img src="./png_new/case12.png" class='img-responsive grayscale'>
                        <div class='text'> Подросток пришел грязный и с порванными вещами. Опять. Похоже, в школе буллинг</div>
                    </a>
                </div>

                <div class="col-md-4 cards case_13" data-case='case_13'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/13-view.html' class="cardin">
                        <img src="./png_new/case13.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток хочет сделать тату и покрасить волосы в изумрудный цвет</div>
                    </a>
                </div>

                <div class="col-md-4 cards case_14" data-case='case_14'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/14-view.html' class="cardin">
                        <img src="./png_new/case14.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток приходит с прогулки все позже, не сообщает куда идет</div>
                    </a>
                </div>

                <div class="col-md-4 cards case_15" data-case='case_15'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/15-view.html' class="cardin">
                        <img src="./png_new/case15.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток подолгу переписывается с кем-то в соц сетях, телефон запаролен</div>
                    </a>
                </div>

                <div class="col-md-4 cards case_16" data-case='case_16'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/16-view.html' class="cardin">
                        <img src="./png_new/case16.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток впервые влюбился</div>
                    </a>
                </div>

                <div class="col-md-4 cards case_17" data-case='case_17'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/17-view.html' class="cardin">
                        <img src="./png_new/case17.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток пришел домой с признаками алкогольного опьянения и запахом сигарет</div>
                    </a>
                </div>

                <div class="col-md-4 cards case_18" data-case='case_18'>
                    <div class='achive'>
                        <img class='y' title='Ты супер-предок' src='/png/achieve_super.png'>
                        <img class='n' title='Ты можешь стать супер-предком, если постараешься' src='/png/achieve_super_gray.png'>
                    </div>
                    <a href='pages/18-view.html' class="cardin">
                        <img src="./png_new/case18.png" class='img-responsive grayscale'>
                        <div class='text'>Подросток говорит, что у них отменили занятия, но вы знаете, что это не правда</div>
                    </a>
                </div>

            </div>
        </section>

        <div id='author'>
            <p><strong>Авторский коллектив игры: </strong>
                <br><b>Руководитель проекта:</b> <a href='https://www.facebook.com/LanaBogat' target="_blank">Светлана Богатырева (ЧФ Just Support) </a>
                <br><b>Разработка:</b> <a href='https://livelogic.kz' target="_blank">Артем Макаренко (студия Li-Lo)</a>
                <br><b>Дизайн:</b> <a href='https://www.facebook.com/tonkonogy' target="_blank">Евгений Тонконогий</a>
                <br><b>Игрофикация:</b> <a href='https://www.facebook.com/miroslava.bronnikova' target="_blank">Мирослава Бронникова</a> <a href='http://why42.ru/' target="_blank">(Клуб Игрофикаторов Why42)</a>
            </p>
            <p>
                <br><b>Партнерский проект:</b> <a href='https://www.soulgames.kz/' target="_blank">Психология через игры</a>
                <br><b><a href='https://stepik.org/course/83073/promo' target="_blank">Подробнее о психологии подростков</b></a>
            </p>
        </div>



    </div>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-42364374-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-42364374-2');
    </script>

</body>

</html>