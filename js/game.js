var Game = {

    w: { //wrappers
        chat: '#chat',
        question: '#questions',
        child: '#child',
        parent: '#parent',
        scale: '.scale',
        /*Main wrapper for scale*/
        scale_parent: "#scale",
        paint: '.logo'
    },


    step_count: 10, //Count of steps
    animate_timer: 1000, //milliseconds
    start_position: 5, //step

    answer_timer: 2000,

    /*Main init function for listeners and positions*/
    init: function() {

        /*reset positions*/
        var step_size = this.getStepSize();

        if (Start) this.start_position = 0;

        var left = step_size * this.start_position;

        if (Start)
            left = 22;

        $(this.w.child).width(step_size).animate({
            left: left,
            // width: step_size
        }, 2000);

        this.paintLogo();

        /*Set listener*/
        this.setListener();

        if (!DataQuestions) return;
        /*Start with questions*/
        this.getQuestion();

        /*Set resize function*/
        this.resize();
    },

    setListener: function() {
        var self = this;

        $(this.w.question).on('click', 'a', function(e) {
            if ($(this).parent().is('.active')) {
                self.question($(this).text(), this);
            }
            return false;
        });
    },

    getAvatar: function() {
        var gender = localStorage.getItem('gender') || "w";

        return "/png/" + gender + ".png";
    },


    /*Questions is pressed*/
    question: function(text, e) {

        var self = this;

        /*Find avatar*/
        var avatar = this.getAvatar();

        var tpl = "<div class='m-message pull-right'><table><tr><td class='bubble bubble-parent'>" + text + "</td><td class='end'><img src='/png/chat_parent_end.png' class='bubble-r-end'></td><td class='avatar'><img src='" + avatar + "'></td></tr></table></div>";
        // var tpl = '<div class="m-message bubble bubble-right pull-right">'+text+'</div>';
        tpl += this.clear();

        /*Append to chat*/
        $(this.w.chat).append(tpl);

        /*Scroll chat*/
        this.scrollChat();

        /*Disable answers*/
        $(this.w.question).removeClass('active');

        /*Get Think And Answer*/
        var data = this.getAnswer(e);
        // console.log(data);

        /*Logic MOVE - > GAME OVER - > GLOBAL COMMENT*/
        if (!data.next && !data.answer) {
            this.comment(data.comment);
            var t = this;
            setTimeout(function() {
                t.gameOver();
            }, 3000);
            // return false;
        }

        /*move the Child*/
        if (!data.answer)
            self.moveChild(data.move);

        if (data.answer) {
            /*Delay, and answer and think*/
            setTimeout(function() {

                /*Show Think*/

                /*clean string value of ansers false*/
                data.answer.a = data.answer.a == 'false' ? '' : data.answer.a;

                /*Show answer*/
                self.answer(data.answer.a, data.answer.t);


                /*move the Child*/
                self.moveChild(data.move);

                setTimeout(function() {
                    /*Show comment*/
                    if (data.comment) self.comment(data.comment);

                    /*if over*/
                    if (!data.next && data.answer.a !== true) {
                        return self.gameOver();
                    }

                    if (data.win)
                        return self.gameOver();
                    else if (!data.next) {
                        return self.gameOver();
                    }

                    /*Next Answers*/
                    if (data.next) self.getQuestion(data.next);

                }, self.answer_timer * 3);


            }, this.answer_timer);
        }
    },

    answer: function(text, think) {

        text = text == 'true' ? '' : text;
        var tpl = "<div class='m-message pull-left'><table><tr><td class='child-comment' colspan='2'><div class='t'>" + think + "</div><img src='/png/comment_end.png'></td></tr><tr><td class='avatar'><img src='/png/" + c + "_child.png'></td><td class='bubble-text' style='display:none;'><img src='/png/chat_child_end.png' class='bubble-l-end'><div class='bubble bubble-child'>" + text + "</div></td></tr></table></div>";
        // var tpl = '<div class="m-message bubble bubble-left pull-left">'+text+'</div>';
        tpl += this.clear();

        /*Append to chat*/
        $(this.w.chat).append(tpl);

        /*Scroll chat*/
        this.scrollChat();

        var t = this;
        /*Show the text*/
        setTimeout(function() {
            $('.m-message:last').find('.bubble-text').fadeIn(1500);

            /*Enable answers*/
            $(t.w.question).addClass('active');

        }, this.answer_timer);




    },

    think: function(text) {
        var tpl = '<div class="think bubble">' + text + '</div>';

        /*append*/
        $(this.w.chat).append(tpl);
        this.scrollChat();
    },

    comment: function(text) {
        text = text || "Хорошая идея!";
        // var tpl = '<div class="comment alert alert-info">'+text+'</div>';
        var tpl = "<div class='m-message pull-left'><table><tr><td class='avatar'><img src='/png/master.png'></td><td class='bubble-text'><img src='/png/chat_master_end.png' class='bubble-m-end'><div class='bubble bubble-master'>" + text + "</div></td></tr></table></div>";
        $(this.w.chat).append(tpl);
        this.scrollChat();
    },

    clear: function() {
        return '<div class="clearfix"></div>';
    },

    scrollChat: function() {
        $(this.w.chat).stop().animate({ scrollTop: $(this.w.chat)[0].scrollHeight }, 1000);
    },

    getStepSize: function() {
        return $(this.w.scale_parent).width() / this.step_count;
    },

    moveChild: function(step) { //-3 or +2
        /*Get cur left position*/
        var curPos = parseInt($(this.w.child).css('left'));

        /*Get step size*/
        var step_size = this.getStepSize();

        var move = 0;

        var gameOver = false;

        var self = this;

        /*Counting*/
        if (step) {

            /*Show bage*/
            if (step > 0) {
                $('.bage.minus').hide();
                $('.bage.plus').text(step).show();
            } else {
                $('.bage.plus').hide();
                $('.bage.minus').text(step).show();
            }

            move = curPos - (step * step_size);

            if (move < step_size) {
                // move = step_size;
                move = 22;
                // self.win();
            }

            if (move == $(window).width()) move = move - step_size;

            if (move > $(window).width()) {
                move = $(window).width();
                gameOver = true;
            }

            // console.log('2move&step', move, step_size);
            $(this.w.child).stop().animate({ left: move }, this.animate_timer, function() {
                self.paintLogo();
                // $(".bage").hide();
            });
        }

    },

    /*Function get question*/

    getQuestion: function(i) {
        i = i || 0;
        var questions = DataQuestions[i];

        /*Get questions list from DB*/
        var html = '',
            q, j;
        for (var i in questions) {
            q = questions[i];

            /*If qe have some variants get Randomly*/
            if (questions[i].length) {
                j = parseInt((Math.random() * (questions[i].length - 1)).toFixed(0));
                q = questions[i][j];
            }
            html += "<a data-move='" + q.m + "' data-next='" + q.n + "' data-comment='" + q.c + "' data-answer='" + q.a + "' data-win='" + q.win + "'>" + q.q + "</a>\n\r";
        }

        /*Put questions on page*/
        $(this.w.question).hide().html(html);
        $(this.w.question).fadeIn();
    },


    /*Function get answer*/
    /*Get answer from clicked question*/
    getAnswer: function(e) {
        var data = $(e).data();
        data.answer = DataAnswers[data.answer];

        return data;
    },

    gameOver: function() {
        var tpl = '<div class="clearfix"></div><div class="end-text"><img src="../img/btn_again.png" class="start" onclick="location.reload();">';
        tpl += "<p class='text-center'><br><a href='/' class='btn btn-default'> <span class='glyphicon glyphicon-th'></span> Перейти к другим ситуациям</a></p>";
        tpl += "<div style='text-align: center;' class='alert for-end'></div></div>";

        // var tpl = '<div class="comment alert alert-danger">Попробуйте еще раз.</div>';
        // $("#chat").fadeOut('slow');
        // $(".m-message, .think, .comment:not(:last), .for-end, .start, .end-text, .smile").remove();
        var self = this;

        /*delete doubles*/
        $(".for-end, img.start, .smile, .end-text").remove();

        $(this.w.chat).append(tpl);
        this.win();
        this.showEndComment();
        this.scrollChat();


    },

    showShare(title, desc, img){
        
        var share = '<div id="ya-share2" style="text-align: center;" class="ya-share2" data-description="'+desc+'" data-image="http://blizkie.kz/'+img+'" data-curtain data-services="vkontakte,facebook,odnoklassniki,telegram,twitter,viber,whatsapp,moimir"></div>';
        $(this.w.chat).append(share);
        
        setTimeout(() => {
            Ya.share2("ya-share2")
        }, 200)

    },

    getCurStep: function() {
        var step_size = parseInt(this.getStepSize());
        var curPos = parseInt($(this.w.child).css('left'));

        var step = parseInt(curPos / step_size);
        return step;
    },

    paintLogo: function() {
        var step = this.getCurStep();
        var countOpacity = 1 / step;

        if (countOpacity == 0.1) countOpacity = 0;

        if (!step || step == 1) countOpacity = 1;

        $(this.w.paint).animate({ opacity: countOpacity }, 2000);
    },

    win: function() {

        /*Count steps*/
        var steps = this.getCurStep();

        if (steps <= 0)
            localStorage.setItem(c, steps);

        var btn = "<a href='javascript:void(0);' onclick='Game.showModal(1);'>Записаться на консультацию</a>";
        btn += "<a href='javascript:void(0);' onclick='Game.showModal(2);'>Психология через игры</a>";
        btn += "<a href='javascript:void(0);' onclick='Game.showModal(3);'>Подписаться на обновления</a>";
        $(this.w.question).fadeOut('fast').delay(500).html(btn).fadeIn('fast');
    },

    showEndComment: function() {
        var step = this.getCurStep();
        var text = '';

        var css = "display: block; margin-left: auto; margin-right: auto;";
        if (step == 5){
             text = "<img src='/png/achieve_normal.png' class='smile img-responsive' style='" + css + "'><br><div class='end-text' style='text-align: center;'>Ваши взаимоотношения с ребенком остались в нейтральной зоне. Нет конфликта, но нет и хорошего контакта. Сыграйте еще раз и попытайтесь найти другую линию поведения.</div><br>";
            this.showShare('Нет конфликта, но нет и хорошего контакта. Сыграйте еще раз', 'Ваши взаимоотношения с ребенком остались в нейтральной зоне. Нет конфликта, но нет и хорошего контакта. Сыграйте еще раз и попытайтесь найти другую линию поведения.', '/png/achieve_normal.png');
        }

        if (step < 5 && step > 0) {
            text = "<img src='/png/achieve_ok.png' class='smile img-responsive' style='" + css + "'><br><div class='end-text' style='text-align: center;'>Вы стали ближе к своему ребенку. Спасибо, продолжайте в том же духе.</div><br>";
            this.showShare('Вы стали ближе к своему ребенку. Спасибо, продолжайте в том же духе.', 'Вы стали ближе к своему ребенку. Спасибо, продолжайте в том же духе.', '/png/achieve_ok.png');
        }

        if (step > 5 && step < 10){ 
            text = "<img src='/png/achieve_sad.png' class='smile img-responsive' style='" + css + "'><br><div class='end-text' style='text-align: center;'>Осторожно! В результате своих действий Вы отдалили от себя ребенка. Сыграйте еще раз и попробуйте что-то улучшить.</div><br>";
            this.showShare('Вы отдалили от себя ребенка. Сыграйте еще раз.', 'Осторожно! В результате своих действий Вы отдалили от себя ребенка. Сыграйте еще раз и попробуйте что-то улучшить.', '/png/achieve_sad.png');
        }

        if (step == 0){
            text = "<img src='/png/achieve_good.png' class='smile img-responsive' style='" + css + "'><br><div class='end-text' style='text-align: center;'>Супер-предок! Поздравляем! Вы справились!</div><br>";
            this.showShare('Супер-предок! Поздравляем! Вы справились!', 'Супер-предок! Поздравляем! Вы справились!', '/png/achieve_good.png');
        }

        if (step >= 10){ 
            text = "<img src='/png/achieve_sad.png' class='smile img-responsive' style='" + css + "'><br>";
            this.showShare('', '', '/png/achieve_sad.png');
        }

        $(this.w.chat).append(text);
        return text;
    },

    getModal: function(title, body, id) {
        var modal = '<div class="modal fade" id="' + id + '" tabindex="-1" role="dialog"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">' + title + '</h4></div><div class="modal-body">' + body + '</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button></div></div></div></div>';
        return modal;
    },

    showModal: function(type) {

        var modal = ".modal#m_" + type;

        var html = title = '';

        html = "<p class='alert alert-success' style='display:none'>Спасибо, ваше сообщение успешно отправлено</p>";
        html += "<p class='alert alert-danger' style='display:none'>Произошла ошибка, попробуйте позже;(</p>";

        switch (type) {
            case 1:
                title = "Записаться на консультацию";

                html += "<form id='consult' class='modal_form'>"
                html += "<div class='form-group'>";
                html += "<input type='text' required name='name' class='form-control' placeholder='Введи имя'>";
                html += "</div>";
                html += "<div class='form-group'>";
                html += "<input type='email' required name='email' class='form-control' placeholder='Введи Email'>";
                html += "</div>";
                html += "<button class='btn btn-success btn-block'>Записаться</button>";
                html += "</form>";
                break;

            case 2:
                location.href = "https://www.soulgames.kz/";
                break;

            case 3:
                title = "Подписаться на обновления";

                html += "<form id='update' class='modal_form'>"
                html += "<div class='form-group'>";
                html += "<input type='text' required  name='name' class='form-control' placeholder='Введи имя'>";
                html += "</div>";
                html += "<div class='form-group'>";
                html += "<input type='email' required name='email' class='form-control' placeholder='Введи Email'>";
                html += "</div>";
                html += "<div class='form-group'>";
                html += "<input type='text' required name='phone' class='form-control' placeholder='Введи телефон'>";
                html += "</div>";
                html += "<button class='btn btn-success btn-block'>Отправить</button>";
                html += "</form>";
                break;
        }

        if (!$(modal).length) {
            $("body").append(this.getModal(title, html, "m_" + type));
        }

        $(modal).modal('show');

        $(".modal_form").unbind('submit');
        $(".modal_form").submit(function(e) {
            e.preventDefault();

            var id = $(this).attr('id');

            var data = { id: id, data: $(this).serializeArray() };

            $.getJSON("/sender.php", data, function(res) {
                if (res == true) {
                    $(".modal").find('.alert-success').show();
                    $(".modal").find('.alert-danger').hide();
                } else {
                    $(".modal").find('.alert-danger').show();
                    $(".modal").find('.alert-success').hide();
                }
            });
        });
    },

    resize: function() {
        var small = $(".case-img").is(':hidden');
        var new_height = $(window).height() - 135;

        console.log("resize to", new_height - 300);

        if (small) {
            // $("#main-window").height(new_height);
            $("#main-window").css({ height: 'auto', "min-height": new_height });

            $("#answer-wrapper").css('padding', '0');

            var c_height = (new_height - 300);
            c_height = c_height < 215 ? 215 : c_height;
            $("#chat").height(c_height);
        } else {
            $("#main-window, #chat").height(new_height);
        }

        var h_a = 300;
        var h_w = $("#main-window").height();
        $(".case-img img").width(h_w - h_a);
    }
};


var DataQuestions; // = [[[{"q":"\u0414\u0430\u0432\u043d\u043e \u0441\u0438\u0434\u0438\u0448\u044c? ","a":"0","m":"0","n":"1","c":"\u041d\u0430 \u0437\u0430\u043a\u0440\u044b\u0442\u044b\u0439 \u0432\u043e\u043f\u0440\u043e\u0441 \u0431\u0443\u0434\u0435\u0442 \u043e\u0434\u043d\u043e\u0441\u043b\u043e\u0436\u043d\u044b\u0439 \u043e\u0442\u0432\u0435\u0442.","win":"false"},{"q":"\u0423\u0440\u043e\u043a\u0438 \u0441\u0434\u0435\u043b\u0430\u043d\u044b?","a":"1","m":"0","n":"1","c":"\u0417\u0430\u043a\u0440\u044b\u0442\u044b\u0439 \u0432\u043e\u043f\u0440\u043e\u0441 - \u0447\u0430\u0449\u0435 \u0432\u0441\u0435\u0433\u043e \u0432\u044b\u0437\u044b\u0432\u0430\u0435\u0442 \u043e\u0434\u043d\u043e\u0441\u043b\u043e\u0436\u043d\u044b\u0439 \u043e\u0442\u0432\u0435\u0442.","win":"false"},{"a":"9","m":"-55","n":"5","c":"Test","win":"false","q":"\u0418\u0433\u043d\u043e\u0440"}],[{"q":"\u041c\u043d\u0435 \u043d\u0443\u0436\u043d\u043e \u0441 \u0442\u043e\u0431\u043e\u0439 \u043f\u043e\u0433\u043e\u0432\u043e\u0440\u0438\u0442\u044c","a":"8","m":"1","n":"3","c":"\u0412\u044b\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0447\u0443\u0432\u0441\u0442\u0432 \u0438 \u043d\u0430\u043c\u0435\u0440\u0435\u043d\u0438\u0439 \u043e\u0442 \u043f\u0435\u0440\u0432\u043e\u0433\u043e \u043b\u0438\u0446\u0430 \u043f\u043e\u043c\u043e\u0433\u0430\u0435\u0442 \u0440\u0430\u0437\u0433\u043e\u0432\u043e\u0440\u0443","win":"false"},{"q":"\u042f \u0445\u043e\u0447\u0443 \u0442\u0435\u0431\u0435 \u0447\u0442\u043e-\u0442\u043e \u0440\u0430\u0441\u0441\u043a\u0430\u0437\u0430\u0442\u044c","a":"8","m":"1","n":"3","c":"\u0412\u044b\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0447\u0443\u0432\u0441\u0442\u0432 \u0438 \u043d\u0430\u043c\u0435\u0440\u0435\u043d\u0438\u0439 \u043e\u0442 \u043f\u0435\u0440\u0432\u043e\u0433\u043e \u043b\u0438\u0446\u0430 \u043f\u043e\u043c\u043e\u0433\u0430\u0435\u0442 \u0440\u0430\u0437\u0433\u043e\u0432\u043e\u0440\u0443","win":"false"}]],[{"q":"\u0412\u044b\u043a\u043b\u044e\u0447\u0438 \u0431\u044b\u0441\u0442\u0440\u043e!","a":"2","m":"-1","n":"2","c":"\u041f\u0440\u0438\u043a\u0430\u0437 \u0432\u044b\u0437\u044b\u0432\u0430\u0435\u0442 \u0441\u043e\u043f\u0440\u043e\u0442\u0438\u0432\u043b\u0435\u043d\u0438\u0435...","win":"false"},{"q":"\u041a\u0430\u0436\u0434\u044b\u0439 \u0434\u0435\u043d\u044c \u043e\u0434\u043d\u043e \u0438 \u0442\u043e\u0436\u0435!","a":"3","m":"-1","n":"false","c":"\u041e\u0442\u043a\u0430\u0437 \u043e\u0442 \u043e\u0442\u0432\u0435\u0442\u0441\u0442\u0432\u0435\u043d\u043d\u043e\u0441\u0442\u0438. \u0412 \u0434\u0430\u043d\u043d\u043e\u043c \u0441\u043b\u0443\u0447\u0430\u0435, \u0440\u043e\u0434\u0438\u0442\u0435\u043b\u044c \u043f\u0440\u0435\u0434\u043f\u043e\u0447\u0435\u043b \u043e\u0442\u0441\u0442\u0440\u0430\u043d\u0438\u0442\u044c\u0441\u044f \u043e\u0442 \u043f\u0440\u043e\u0431\u043b\u0435\u043c\u044b \u0438 \u043d\u0435 \u0440\u0435\u0448\u0430\u0442\u044c \u0435\u0435.","win":"false"}],[{"q":"\u0414\u0430\u0442\u044c \u043f\u043e\u0434\u0437\u0430\u0442\u044b\u043b\u044c\u043d\u0438\u043a","a":"4","m":"-4","n":"false","c":"\u0424\u0438\u0437\u0438\u0447\u0435\u0441\u043a\u043e\u0435 \u043d\u0430\u0441\u0438\u043b\u0438\u0435 \u043d\u0435 \u043f\u0440\u0438\u0435\u043c\u043b\u0435\u043c\u043e!","win":"false"},{"q":"\u0412\u044b\u0434\u0435\u0440\u043d\u0443\u0442\u044c \u043a\u043e\u043c\u043f\u044c\u044e\u0442\u0435\u0440 \u0438\u0437 \u0441\u0435\u0442\u0438","a":"5","m":"-3","n":"false","c":"\u0414\u0435\u043c\u043e\u043d\u0441\u0442\u0440\u0430\u0446\u0438\u044f \u0441\u0438\u043b\u044b \u0432\u044b\u0437\u044b\u0432\u0430\u0435\u0442 \u0430\u0433\u0440\u0435\u0441\u0441\u0438\u044e!","win":"false"},{"q":"\u041f\u043e\u0442\u043e\u043c\u0443 \u0447\u0442\u043e!","a":"6","m":"-2","n":"false","c":"\u041e\u0442\u043a\u0430\u0437 \u043e\u0442 \u043a\u043e\u043d\u0442\u0430\u043a\u0442\u0430 \u0443\u0441\u0443\u0433\u0443\u0431\u043b\u044f\u0435\u0442 \u043a\u043e\u043d\u0444\u043b\u0438\u043a\u0442!","win":"false"},{"q":"\u0421\u0438\u0434\u0435\u0442\u044c \u0434\u043e\u043b\u0433\u043e \u0432\u0440\u0435\u0434\u043d\u043e","a":"7","m":"-1","n":"false","c":"\u041c\u043e\u0440\u0430\u043b\u0438\u0437\u0430\u0442\u043e\u0440\u0441\u0442\u0432\u043e - \u043d\u0435 \u043b\u0443\u0447\u0448\u0438\u0439 \u0441\u043f\u043e\u0441\u043e\u0431 \u0432\u0435\u0434\u0435\u043d\u0438\u044f \u0431\u0435\u0441\u0435\u0434\u044b","win":"false"}],[{"q":"\u042f \u043d\u0435 \u043f\u043e\u043d\u0438\u043c\u0430\u044e, \u043f\u043e\u0447\u0435\u043c\u0443 \u0442\u044b \u0441\u0442\u043e\u043b\u044c\u043a\u043e \u0432\u0440\u0435\u043c\u0435\u043d\u0438 \u043f\u0440\u043e\u0432\u043e\u0434\u0438\u0448\u044c \u0437\u0430 \u043a\u043e\u043c\u043f\u044c\u044e\u0442\u0435\u0440\u043e\u043c?","a":"10","m":"-1","n":"2","c":"\u0412\u043e\u043f\u0440\u043e\u0441 \u0432 \u0432\u0438\u0434\u0435 \u043e\u0431\u0432\u0438\u043d\u0435\u043d\u0438\u044f \u0432\u0435\u0434\u0451\u0442 \u043a \u043a\u043e\u043d\u0444\u043b\u0438\u043a\u0442\u0443.","win":"false"},{"q":"\u041c\u044b \u0441 \u0442\u043e\u0431\u043e\u0439 \u0434\u0430\u0432\u043d\u043e \u043d\u0435 \u0440\u0430\u0437\u0433\u043e\u0432\u0430\u0440\u0438\u0432\u0430\u043b\u0438","a":"11","m":"1","n":"4","c":"\u0420\u0430\u0432\u0435\u043d\u0441\u0442\u0432\u043e \u043f\u043e\u0437\u0438\u0446\u0438\u0439 \u0438 \u043e\u0442\u043a\u0440\u044b\u0442\u043e\u0441\u0442\u044c \u0432 \u0440\u0430\u0437\u0433\u043e\u0432\u043e\u0440\u0435 \u043f\u043e\u043c\u043e\u0433\u0430\u044e\u0442 \u043f\u0440\u0438\u0432\u043b\u0435\u0447\u044c \u0432\u043d\u0438\u043c\u0430\u043d\u0438\u0435 \u0440\u0435\u0431\u0451\u043d\u043a\u0430.","win":"false"},[{"q":"\u0420\u0430\u0441\u0441\u043a\u0430\u0437\u0430\u0442\u044c \u043e \u0440\u0430\u0431\u043e\u0442\u0435","a":"9","m":"-1","n":"false","c":"\u041f\u043e\u0434\u0440\u043e\u0441\u0442\u043a\u0430 \u043c\u0430\u043b\u043e \u0438\u043d\u0442\u0435\u0440\u0435\u0441\u0443\u044e\u0442 \u0442\u0435\u043c\u044b, \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u0435\u0433\u043e \u043d\u0435 \u043a\u0430\u0441\u0430\u044e\u0442\u0441\u044f, \u043e\u0441\u043e\u0431\u0435\u043d\u043d\u043e \u0435\u0441\u043b\u0438 \u0432\u0430\u0436\u043d\u044b\u0435 \u0435\u043c\u0443 \u0442\u0435\u043c\u044b \u0438\u0433\u043d\u043e\u0440\u0438\u0440\u0443\u0440\u044e\u0442\u0441\u044f","win":"false"},{"q":"\u0420\u0430\u0441\u0441\u043a\u0430\u0437\u0430\u0442\u044c \u043e \u0441\u0435\u043c\u044c\u0435","a":"9","m":"-1","n":"false","c":"\u041f\u043e\u0434\u0440\u043e\u0441\u0442\u043a\u0430 \u043c\u0430\u043b\u043e \u0438\u043d\u0442\u0435\u0440\u0435\u0441\u0443\u044e\u0442 \u0442\u0435\u043c\u044b, \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u0435\u0433\u043e \u043d\u0435 \u043a\u0430\u0441\u0430\u044e\u0442\u0441\u044f, \u043e\u0441\u043e\u0431\u0435\u043d\u043d\u043e \u0435\u0441\u043b\u0438 \u0432\u0430\u0436\u043d\u044b\u0435 \u0435\u043c\u0443 \u0442\u0435\u043c\u044b \u0438\u0433\u043d\u043e\u0440\u0438\u0440\u0443\u0440\u044e\u0442\u0441\u044f","win":"false"}]],[{"q":"\u042f \u0445\u043e\u0447\u0443 \u0441\u043a\u0430\u0437\u0430\u0442\u044c \u0442\u0435\u0431\u0435, \u0447\u0442\u043e \u0441\u0438\u043b\u044c\u043d\u043e \u0432\u043e\u043b\u043d\u0443\u044e\u0441\u044c, \u0447\u0442\u043e \u0442\u044b \u0441\u0442\u043e\u043b\u044c\u043a\u043e \u0432\u0440\u0435\u043c\u0435\u043d\u0438 \u043f\u0440\u043e\u0432\u043e\u0434\u0438\u0448\u044c \u0437\u0430 \u043a\u043e\u043c\u043f\u044c\u044e\u0442\u0435\u0440\u043e\u043c","a":"12","m":"1","n":"false","c":"\u0420\u0435\u0431\u0451\u043d\u043a\u0443 \u0432\u0430\u0436\u043d\u043e \u0447\u0443\u0432\u0441\u0442\u0432\u043e\u0432\u0430\u0442\u044c \u0441\u0432\u043e\u044e \u0437\u043d\u0430\u0447\u0438\u043c\u043e\u0441\u0442\u044c \u0434\u043b\u044f \u0440\u043e\u0434\u0438\u0442\u0435\u043b\u044f.","win":"true"},{"q":"\u0414\u0430\u0432\u0430\u0439 \u0434\u043e\u0433\u043e\u0432\u043e\u0440\u0438\u043c\u0441\u044f, \u043a\u0430\u043a \u0442\u044b \u0431\u0443\u0434\u0435\u0448\u044c \u0441\u0438\u0434\u0435\u0442\u044c \u0437\u0430 \u043a\u043e\u043c\u043f\u044c\u044e\u0442\u0435\u0440\u043e\u043c. \u0427\u0442\u043e \u0434\u0443\u043c\u0430\u0435\u0448\u044c?","a":"14","m":"2","n":"false","c":"\u041f\u0430\u0440\u0442\u043d\u0451\u0440\u0441\u043a\u0430\u044f \u043f\u043e\u0437\u0438\u0446\u0438\u044f \u043f\u043e\u043c\u043e\u0433\u0430\u0435\u0442 \u0443\u0441\u0442\u0430\u043d\u043e\u0432\u0438\u0442\u044c \u0438 \u0437\u0430\u043a\u0440\u0435\u043f\u0438\u0442\u044c \u043a\u043e\u043d\u0442\u0430\u043a\u0442 \u0441 \u0440\u0435\u0431\u0451\u043d\u043a\u043e\u043c.","win":"true"}],[{"q":"\u042d\u0442\u043e \u043d\u043e\u0432\u044b\u0439 \u0431\u043b\u043e\u043a","win":"false","c":"\u0435\u0443\u044b\u0435","n":"0","m":"+5","a":"0"}],[{"q":"\u0430\u0440\u043f\u0432\u0430\u043f\u0432\u0430\u043f\u0432\u0430\u043f","win":"false","c":"\u0432\u0430\u043f\u0432\u0430\u043f","n":"2","m":"4","a":"2"}]];

var DataAnswers;
/*[
	{a: "Два часа", t: "Опять хочет следить за каждым шагом"},
	{a: "Не все", t: "Никакой свободы"},
	{a: "Почему?", t: "Опять командует. Надоело!"},
	{a: false, t: "Начинается!"},
	{a: false, t: "Мне страшно..."},//4
	{a: false, t: "Скорее бы вырасти и отомстить!"},//5
	{a: false, t: "Всем плевать на моё мнение"},//6
	{a: false, t: "А жить вообще вредно!"},//7
	{a: "Ммм", t: "Неужели?"},//8
	{a: "Ты всё?", t: "Ну круто, а мне что?"},//9
	{a: "Все так делают! Все мои друзья так делают! Почему мне нельзя?", t: "Опять, ну сколько можно!"},//10
	{a: "Ну, да", t: "Ок, давай!"},//11
	{a: true, t: "Я важен, за меня волнуются"},//12
	{a: true, t: "Всегда есть что-то важнее меня!"},//13
	{a: true, t: "Моё мнение учитывается!"},//14
];*/


$(document).ready(function() {

    c = c || "case_1";

    if (!Start) {
        $.getJSON('/ajaxAdmin/db/' + c + '.questions.json', function(q) {
            DataQuestions = q;
            $.getJSON("/ajaxAdmin/db/" + c + ".answers.json", function(a) {
                DataAnswers = a;
                $(".menu-a").fadeIn('slow', function() {
                    $("#p2").width($(".header").width() - $("#p1").width());
                    Game.init();
                })
            });
        });
    } else {

        $("#p2").width($(".header").width() - $("#p1").width());
        Game.init();

        /*Get Local Storages data*/

    }

    // $("#p2").width($(".header").width()-$("#p1").width());		
    // Game.init();

    $(window).resize(function() {
        Game.resize();
    });

})