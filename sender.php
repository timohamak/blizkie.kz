<?php
	$id = $_REQUEST['id'];
	$data = $_REQUEST['data'];

	if($id == 'consult') $title = "Запись на консультацию";
	else $title = "Подписка на обновления";

	if(!$id) die(json_encode(false));

	require $_SERVER["DOCUMENT_ROOT"] . '/mailer/class/SMTPMailer.php';
	$mail = new SMTPMailer();

	$mail->addTo('info@blizkie.kz');

	$mail->Subject('Близкие: сообщение из формы обратной связи.');

	$html = '<div style="background: #fff; text-align: center;">
			<div style="width: auto; max-width: 600px; background: #f2fbe8; padding: 25px; border-radius: 8px; font-family: Verdana; font-size: 16px; border: 2px solid #cae5aa;">
				<img src="http://blizkie.kz/img/logo.png" style="">
				<h3 style="color: #8BC34A;"">Запись на консультацию</h3>';
	
	foreach($data as $field){
		if($field['name'] ==  'name')
			$html .= '<p style=""><strong>Меня зовут '.$field['value'].'!</strong></p>';

		if($field['name'] == 'email')
			$html .= '<p style="">Моя электронная почта: <span style="text-decoration: underline; font-weight: bold;">'.$field['value'].'</span></p>';

		if($field['name'] == 'phone')
			$html .= '<p style="">Мой телефон: <span style="text-decoration: underline; font-weight: bold;">'.$field['value'].'</span></p>';
	}

	$html .='<p style="font-size: 14px; font-style: italic;">Сообщение отправлено автоматически через сайт blizkie.kz</p>
			</div>
		</div>';

	$mail->Body($html);

	if ($mail->Send())
		die(json_encode(true));
	else
		die(json_encode(false));





	die(json_encode(true));
?>